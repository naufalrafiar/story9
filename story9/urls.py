from django.urls import path
from .views import index, data


appname = 'Story9'

urlpatterns = [
   path('', index, name = 'indexBook'),
   path('data/', data, name='dataBook'),
]